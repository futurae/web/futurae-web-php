<?php
namespace Unit;

include(dirname(__FILE__) . "/../../src/Web.php");

class FuturaeTest extends \PHPUnit_Framework_TestCase
{
    const WID = "FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
    const WRONG_WID = "FIDXXXXXXXXXXXXXXXXY";
    const WKEY = "futuraegeneratedsharedwebapplicationkey.";
    const SKEY = "useselfgeneratedhostapplicationsecretkey";
    const USERNAME = "testusername";

    public function setUp()
    {
        $request_sig = \Futurae\Web::signRequest(
            self::WID,
            self::WKEY,
            self::SKEY,
            self::USERNAME
        );
        list($auth_sig, $valid_app_sig) = explode(':', $request_sig);
        $request_sig = \Futurae\Web::signRequest(
            self::WID,
            self::WKEY,
            "invalidinvalidinvalidinvalidinvalidinvalid",
            self::USERNAME
        );

        list($auth_sig, $invalid_app_sig) = explode(':', $request_sig);
        $this->valid_app_sig = $valid_app_sig;
        $this->invalid_app_sig = $invalid_app_sig;
        $this->valid_future_response = Helpers::futureResponse(self::WID, self::WKEY, self::USERNAME);
    }

    public function testNonNull()
    {
        $this->assertNotEquals(
            \Futurae\Web::signRequest(
                self::WID,
                self::WKEY,
                self::SKEY,
                self::USERNAME
            ),
            null
        );
    }

    public function testEmptyUsername()
    {
        $this->assertEquals(
            \Futurae\Web::signRequest(
                self::WID,
                self::WKEY,
                self::SKEY,
                ""
            ),
            \Futurae\Web::ERR_USER
        );
    }

    public function testInvalidUsername()
    {
        $this->assertEquals(
            \Futurae\Web::signRequest(
                self::WID,
                self::WKEY,
                self::SKEY,
                "in|valid"
            ),
            \Futurae\Web::ERR_USER
        );
    }

    public function testInvalidWid()
    {
        $this->assertEquals(
            \Futurae\Web::signRequest(
                "invalid",
                self::WKEY,
                self::SKEY,
                self::USERNAME
            ),
            \Futurae\Web::ERR_WID
        );
    }

    public function testInvalidWkey()
    {
        $this->assertEquals(
            \Futurae\Web::signRequest(
                self::WID,
                "invalid",
                self::SKEY,
                self::USERNAME
            ),
            \Futurae\Web::ERR_WKEY
        );
    }

    public function testInvalidSkey()
    {
        $this->assertEquals(
            \Futurae\Web::signRequest(
                self::WID,
                self::WKEY,
                "invalid",
                self::USERNAME
            ),
            \Futurae\Web::ERR_SKEY
        );
    }

    public function testInvalidResponse()
    {
        $invalid_response = "AUTH|INVALID|SIG";
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $invalid_response . ":" . $this->valid_app_sig
            ),
            null
        );
    }

    public function testExpiredResponse()
    {
        $expired_response = "AUTH|dGVzdHVzZXJ8RElYWFhYWFhYWFhYWFhYWFhYWFh8MTMwMDE1Nzg3NA==|cb8f4d60ec7c261394cd5ee5a17e46ca7440d702";
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $expired_response . ":" . $this->valid_app_sig
            ),
            null
        );
    }

    public function testFutureResponse()
    {
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $this->valid_future_response . ":" . $this->valid_app_sig
            ),
            self::USERNAME
        );
    }

    public function testFutureInvalidAppResponse()
    {
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $this->valid_future_response . ":" . $this->invalid_app_sig
            ),
            null
        );
    }

    public function testFutureBadParamsApp()
    {
        $invalid_params = Helpers::badParamsApp(self::WID, self::SKEY, self::USERNAME);
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $this->valid_future_response . ":" . $invalid_params
            ),
            null
        );
    }

    public function testFutureBadParamsResponse()
    {
        $invalid_response_params = Helpers::badParamsResponse(self::WID, self::WKEY, self::USERNAME);
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WID,
                self::WKEY,
                self::SKEY,
                $invalid_response_params . ":" . $this->valid_app_sig
            ),
            null
        );
    }

    public function testFutureResponseInvalidWid()
    {
        $this->assertEquals(
            \Futurae\Web::verifyResponse(
                self::WRONG_WID,
                self::WKEY,
                self::SKEY,
                $this->valid_future_response . ":" . $this->valid_app_sig
            ),
            null
        );
    }
}
