## Overview

**futurae-web-php** - Provides the Futurae Web PHP helpers to be integrated in your PHP web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.

## Installation using Composer
To use **futurae-web-php** in your project, you can install it using [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos), with the following steps:

Add this repository to your `composer.json`, and the package to the required packages:

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:futurae/web/futurae-web-php.git"
        }
    ],
    "require": {
      "futurae/futurae-web-php": "dev-master"
    }
}
```

Install the dependencies:

```bash
$ composer install
```

This library is autoloaded, so you can autoload the vendor directory:

```php
require __DIR__ . '/vendor/autoload.php';
```

## Dev Installation

```
$ git clone git@gitlab.com:futurae/web/futurae-web-php.git
$ cd futurae-web-php
$ composer install
```

## Unit Tests

```
./vendor/bin/phpunit -c ./phpunit.xml
```

## Example

Fill in your Futurae Web credentials in the `example/index.php` file and then run:

```
php -S localhost:8080 -t example/
```

Finally, visit `http://localhost:8080?username=testusername`

## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
