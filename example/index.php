<?php

/*
 * Example of Futurae Web PHP
 */

require_once '../src/Web.php';

define('WID', '');
define('WKEY', '');
define('SKEY', '');
define('HOST', '');

echo '<!DOCTYPE html>';
echo '<html>';
echo '<head>';
echo '  <title>Futurae Authentication Prompt</title>';
echo '  <meta name="viewport" content="width=device-width, initial-scale=1">';
echo '  <meta http-equiv="X-UA-Compatible" content="IE=edge">';
echo '</head>';
echo '<body>';
echo '  <h1>Futurae Web PHP Example</h1>';
echo '  <script type="text/javascript" src="Futurae-Web-SDK-v1.js"></script>';

/*
 * Verify Futurae's response
 */
if (isset($_POST['sig_response'])) {
    $resp = Futurae\Web::verifyResponse(WID, WKEY, SKEY, $_POST['sig_response']);
    echo 'Hi, ' . $resp . '<br>';
}
/*
 * Generate the request signature and load the Futurae iframe
 */
else {
    $params = array();
    parse_str($_SERVER['QUERY_STRING'], $params);

    $sig_request = Futurae\Web::signRequest(WID, WKEY, SKEY, $params['username']);

    echo '<iframe id="futurae_widget"';
    echo '        title="Two-Factor Authentication"';
    echo '        data-host="'.  HOST . '"';
    echo '        data-sig-request="'. $sig_request . '"';
    echo '        data-lang="en"';
    echo '        frameborder="0"';
    echo '        style="width: 100%; height: 800px;" allow="microphone" ></iframe>';
}

echo '</body';
echo '</html>';

?>
