<?php
namespace Futurae;

/*
 * Futurae Web PHP
 */
class Web
{
    const REQUEST_PREFIX = "REQ";
    const APP_PREFIX = "APP";
    const AUTH_PREFIX = "AUTH";

    const FUTURAE_EXPIRY = 600;
    const APP_EXPIRY = 3600;

    const WID_LEN = 36;
    const WKEY_LEN = 40;
    const SKEY_LEN = 40;

    const ERR_USER = 'ERR|The username specified is invalid.';
    const ERR_WID = 'ERR|The Futurae Web ID specified is invalid.';
    const ERR_WKEY = 'ERR|The Futurae Web Key specified is invalid.';
    const ERR_SKEY = 'ERR|The Secret Key specified must be at least 40 characters.';

    private static function signVals($key, $vals, $prefix, $expire, $time = null, $algo = "sha256")
    {
        $exp = ($time ? $time : time()) + $expire;
        $val = $vals . '|' . $exp;
        $b64 = base64_encode($val);
        $cookie = $prefix . '|' . $b64;

        $sig = hash_hmac($algo, $cookie, $key);

        return $cookie . '|' . $sig;
    }

    private static function parseVals($key, $val, $prefix, $wid, $time = null, $algo = "sha256")
    {
        $ts = ($time ? $time : time());

        $parts = explode('|', $val);
        if (count($parts) !== 3) {
            return null;
        }

        list($u_prefix, $u_b64, $u_sig) = $parts;
        $sig = hash_hmac($algo, $u_prefix . '|' . $u_b64, $key);

        if (hash_hmac($algo, $sig, $key) !== hash_hmac($algo, $u_sig, $key)) {
            return null;
        }
        if ($u_prefix !== $prefix) {
            return null;
        }

        $cookie_parts = explode('|', base64_decode($u_b64));
        if (count($cookie_parts) !== 3) {
            return null;
        }

        list($u_wid, $user, $exp) = $cookie_parts;
        if ($u_wid !== $wid) {
            return null;
        }

        if ($ts >= intval($exp)) {
            return null;
        }

        return $user;
    }

    public static function signRequest($wid, $wkey, $skey, $username, $time = null)
    {
      if (!isset($username) ||
          strlen($username) === 0 ||
          strpos($username, '|') !== false) {
            return self::ERR_USER;
        }
        if (!isset($wid) || strlen($wid) !== self::WID_LEN) {
            return self::ERR_WID;
        }
        if (!isset($wkey) || strlen($wkey) < self::WKEY_LEN) {
            return self::ERR_WKEY;
        }
        if (!isset($skey) || strlen($skey) < self::SKEY_LEN) {
            return self::ERR_SKEY;
        }

        $vals = $wid . '|' . $username;
        $auth_sig = self::signVals($wkey, $vals, self::REQUEST_PREFIX, self::FUTURAE_EXPIRY, $time);
        $app_sig = self::signVals($skey, $vals, self::APP_PREFIX, self::APP_EXPIRY, $time);

        return $auth_sig . ':' . $app_sig;
    }

    public static function verifyResponse($wid, $wkey, $skey, $sig_response, $time = null)
    {
        list($auth_sig, $app_sig) = explode(':', $sig_response);
        $auth_user = self::parseVals($wkey, $auth_sig, self::AUTH_PREFIX, $wid, $time);
        $app_user = self::parseVals($skey, $app_sig, self::APP_PREFIX, $wid, $time);

        if ($auth_user !== $app_user) {
            return null;
        }

        return $auth_user;
    }
}
