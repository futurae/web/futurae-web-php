<?php
namespace Unit;

class Helpers
{
  public static function badParamsApp($wid, $skey, $username)
  {
    $vals = array($wid, $username, self::futureExpiry(), 'bad_param');
    $cookie = self::cookie('APP', $vals);
    $sig = hash_hmac('sha256', $cookie, $skey);
    return $cookie . '|' . $sig;
  }

  public static function badParamsResponse($wid, $skey, $username)
  {
    $vals = array($wid, $username, self::futureExpiry(), 'bad_param');
    $cookie = self::cookie('AUTH', $vals);
    $sig = hash_hmac('sha256', $cookie, $wkey);
    return $cookie . '|' . $sig;
  }

  public static function expiredResponse($wid, $wkey, $username)
  {
    $vals = array($wid, $username, self::expiredExpiry());
    $cookie = self::cookie('AUTH', $vals);
    $sig = hash_hmac('sha256', $cookie, $wkey);
    return $cookie . '|' . $sig;
  }

  public static function futureResponse($wid, $wkey, $username)
  {
    $vals = array($wid, $username, self::futureExpiry());
    $cookie = self::cookie('AUTH', $vals);
    $sig = hash_hmac('sha256', $cookie, $wkey);
    return $cookie . '|' . $sig;
  }

  private static function cookie($prefix, $vals)
  {
    return $prefix . '|' . base64_encode(implode("|", $vals));
  }

  private static function futureExpiry()
  {
    return ($time ? $time : time()) + 600;
  }

  private static function expiredExpiry()
  {
    return ($time ? $time : time()) - 600;
  }
}
